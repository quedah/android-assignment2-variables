import java.time.LocalDate
import java.time.Month
public class Song(){
//Song File with various attributes of the object song
    var songTitle: String = "Mambo Dhuterere - Ahuwerere" 					//The title of the song
    var artist: String = "Mambo Dhuterere" 									//The name of the person who sang the song
    var albumTitle: String = "Dare Guru"  									//Title of the album
    var trackNumber: Int = 5 												//Track number on the album
    var yearOfRelease: LocalDate = LocalDate.of(2019, Month.August, 27)		//The date the song was released
    var genre: String = "Gospel" 											//Genre of the song
    var rating: Float = 4.8f 												//Rating of the song
    var isHitSong: Boolean = true											//Whether the song is a hit song
    var recordingStudioName: String? = null									//The name of Studio that recorded the song
    var numberOfDownloads: Long = 788_789L 									//Number of times the song was downloaded
    var songGrade: Char = 'A' 												//The grade of the song 'A' being the first grade


	//Print each attribute of the Song object
    println(songTitle)
    println(artist)
    println(albumTitle)
    println(trackNumber)
    println(yearOfRelease)
    println(genre)
    println(rating)
    println(isHitSong)
    println(recordingStudioName)
    println(numberOfDownloads)
    println(songGrade)
    }
}